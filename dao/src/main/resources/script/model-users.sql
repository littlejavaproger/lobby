CREATE TABLE users_tbl (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  password varchar(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UK_avh1b2ec82audum2lyjx2p1ws (name)
);

CREATE TABLE roles_tbl (
  id int(11) NOT NULL AUTO_INCREMENT,
  role_name varchar(255) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE user_role_tbl (
  id_user int(11) NOT NULL,
  id_role int(11) NOT NULL,
  FOREIGN KEY (id_user) REFERENCES users_tbl (id),
  FOREIGN KEY (id_role) REFERENCES roles_tbl (id)
);

INSERT INTO users_tbl (id,name,password) VALUES
  (1,'admin','admin'),
  (2,'user','user');

INSERT INTO roles_tbl (id,role_name) VALUES
  (1,'ROLE_ADMIN'),
  (2,'ROLE_USER');

INSERT INTO user_role_tbl (id_user, id_role) VALUES
  (1, 1),
  (1, 2),
  (2, 2);
