package com.getjavajob.nesterenkom.lobby.dao;

import com.getjavajob.nesterenkom.lobby.model.Role;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<Role, Long> {
}
