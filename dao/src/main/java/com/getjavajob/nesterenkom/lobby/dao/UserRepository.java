package com.getjavajob.nesterenkom.lobby.dao;

import com.getjavajob.nesterenkom.lobby.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {

    @Query("select u from User u where u.name=?1 and u.password=?2")
    User login(String name, String password);

    @Query("select u from User u where u.name=?1 and u.password=?2")
    User findByNameAndPassword(String name, String password);

    @Query("SELECT u FROM User u WHERE u.name=?1")
    User findUserByName(String name);

    List<User> findAll();
}
