package com.getjavajob.nesterenkom.lobby.dao;

import com.getjavajob.nesterenkom.lobby.model.User;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class UserRepositoryTest extends DataBaseTestSupport {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void findByIdTest() {
        User expected1 = USER1;
        User actual1 = userRepository.findOne(1L);
        User expected2 = USER2;
        User actual2 = userRepository.findOne(2L);

        assertThat(actual1.getName(), is(expected1.getName()));
        assertThat(actual1.getPassword(), is(expected1.getPassword()));
        Assert.assertNotNull(actual1.getName());
        assertThat(actual2.getId(), is(expected2.getId()));
    }

    @Test
    public void findAllTest() {
        List<User> expected = Arrays.asList(USER1, USER2);
        List<User> actual = userRepository.findAll();

        assertThat(actual.size(), is(expected.size()));
    }

    @Test
    public void addUseryTest() {
        User expected = new User();
        expected.setId(5L);
        expected.setName("Player");
        expected.setPassword("");
        User actual = userRepository.save(expected);

        assertThat(expected, is(actual));
        Assert.assertNotNull(actual.getId());
    }

    @Test
    public void removeUserTest() {
        userRepository.delete(USER1);

        Assert.assertNull(userRepository.findOne(1L));
    }

    @Test
    public void removeByIdTest() {
        userRepository.delete(1L);

        Assert.assertNull(userRepository.findOne(1L));
    }

    @Test
    public void findByNameAndPasswordTest() {
        User expected = USER1;
        User actual = userRepository.findByNameAndPassword(USER1.getName(), USER1.getPassword());

        assertThat(expected.getName(), is(actual.getName()));
        assertThat(expected.getPassword(), is(actual.getPassword()));
        assertThat(expected, is(actual));
    }

    @Test
    public void findByNameTest() {
        User expected = USER1;
        User actual = userRepository.findUserByName(USER1.getName());

        assertThat(expected.getName(), is(actual.getName()));
        assertThat(expected.getPassword(), is(actual.getPassword()));
        assertThat(expected, is(actual));
    }

    @Test
    public void loginTest() {
        User expected = USER1;
        User actual = userRepository.login(USER1.getName(), USER1.getPassword());

        assertThat(expected.getName(), is(actual.getName()));
        assertThat(expected.getPassword(), is(actual.getPassword()));
        assertThat(expected, is(actual));
    }
}
