package com.getjavajob.nesterenkom.lobby.dao;

import com.getjavajob.nesterenkom.lobby.config.DataBaseTestConfiguration;
import com.getjavajob.nesterenkom.lobby.model.Role;
import com.getjavajob.nesterenkom.lobby.model.TypeRole;
import com.getjavajob.nesterenkom.lobby.model.User;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = DataBaseTestConfiguration.class)
@Rollback
@EnableTransactionManagement
@Transactional
public class DataBaseTestSupport {
    protected static User USER1;
    protected static User USER2;
    protected static Role ROLE1;
    protected static Role ROLE2;

    @Before
    public void initExpectedData() {
        init();
    }

    private void init() {
        ROLE1 = new Role();
        ROLE1.setId(1L);
        ROLE1.setRoleName(TypeRole.ROLE_USER);

        ROLE2 = new Role();
        ROLE2.setId(2L);
        ROLE2.setRoleName(TypeRole.ROLE_ADMIN);

        USER1 = new User();
        USER1.setId(1L);
        USER1.setName("User1");
        USER1.setPassword("User1");
        Set<Role> roles1 = new HashSet<>();
        roles1.add(ROLE1);
        roles1.add(ROLE2);
        USER1.setRoles(roles1);

        Set<User> users1 = new HashSet<>();
        users1.add(USER1);
        users1.add(USER2);
        ROLE1.setUsers(users1);

        USER2 = new User();
        USER2.setId(2L);
        USER2.setName("User2");
        USER2.setPassword("User2");
        Set<Role> roles2 = new HashSet<>();
        roles2.add(ROLE1);
        USER2.setRoles(roles2);

        Set<User> users2 = new HashSet<>();
        users2.add(USER1);
        ROLE2.setUsers(users2);
    }
}
