INSERT INTO users_tbl (id,name,password) VALUES
  (1,'User1','User1'),
  (2,'User2','User2');

INSERT INTO roles_tbl (id,role_name) VALUES
  (1,'ROLE_ADMIN'),
  (2,'ROLE_USER');

INSERT INTO user_role_tbl (id_user, id_role) VALUES
  (1, 1),
  (1, 2),
  (2, 2);
