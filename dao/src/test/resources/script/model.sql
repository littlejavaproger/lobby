CREATE TABLE users_tbl (
  id int(11) NOT NULL AUTO_INCREMENT,
  name varchar(255) DEFAULT NULL,
  password varchar(255) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UK_avh1b2ec82audum2lyjx2p1ws (name)
);

CREATE TABLE roles_tbl (
  id int(11) NOT NULL AUTO_INCREMENT,
  role_name varchar(255) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE user_role_tbl (
  id_user int(11) NOT NULL,
  id_role int(11) NOT NULL,
  FOREIGN KEY (id_user) REFERENCES users_tbl (id),
  FOREIGN KEY (id_role) REFERENCES roles_tbl (id)
);