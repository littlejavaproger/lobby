package com.getjavajob.nesterenkom.lobby.service;

import com.getjavajob.nesterenkom.lobby.model.*;
import org.junit.Before;

import java.util.*;

public class LobbyServiceSupportTest {
    protected static UUID UUID1;
    protected static Role ROLE;
    protected static Lobby LOBBY1;
    protected static Lobby LOBBY2;
    protected static User USER_HOST;
    protected static User USER1;
    protected static User USER2;
    protected static User USER3;
    protected static User USER4;

    @Before
    public void init() {
        UUID1 = java.util.UUID.randomUUID();
        initRole();
        initUser();
        initLobby();
    }

    private void initLobby() {
        LOBBY1 = new Lobby();
        LOBBY1.setUuid(UUID1);
        System.out.println("before uuid LOBBY1 [" + LOBBY1.getUuid() + "]");
        LOBBY1.setName("Lobby1");
        LOBBY1.setStatus(Status.RECRUITED_PARTICIPANTS);
        LOBBY1.setType(TypeLobby.TWO_PLAYERS);
        LOBBY1.setHost(USER_HOST);
        LOBBY2 = new Lobby();
        LOBBY2.setUuid(UUID1);
        LOBBY2.setName("Lobby2");
        System.out.println("before uuid LOBBY2 [" + LOBBY2.getUuid() + "]");
        LOBBY2.setStatus(Status.RECRUITED_PARTICIPANTS);
        LOBBY2.setType(TypeLobby.FOUR_PLAYERS);
        LOBBY2.setHost(USER_HOST);
        List<User> users2 = new ArrayList<>();
        users2.add(USER_HOST);
        LOBBY2.setPlayers(users2);
    }

    private void initRole() {
        ROLE = new Role();
        ROLE.setRoleName(TypeRole.ROLE_USER);
    }

    private void initUser() {
        USER_HOST = new User();
        USER_HOST.setId(1L);
        USER_HOST.setName("HOST");
        USER_HOST.setPassword("password");
        Set<Role> roles = new HashSet<>();
        roles.add(ROLE);
        USER_HOST.setRoles(roles);

        USER1 = new User();
        USER1.setId(2L);
        USER1.setName("USER1");
        USER1.setPassword("USER1");
        Set<Role> roles1 = USER1.getRoles();
        roles1.add(ROLE);
        USER1.setRoles(roles1);

        USER2 = new User();
        USER2.setId(3L);
        USER2.setName("USER2");
        USER2.setPassword("USER2");
        Set<Role> roles2 = USER2.getRoles();
        roles2.add(ROLE);
        USER2.setRoles(roles2);

        USER3 = new User();
        USER3.setId(4L);
        USER3.setName("USER3");
        USER3.setPassword("USER3");
        Set<Role> roles3 = USER3.getRoles();
        roles3.add(ROLE);
        USER3.setRoles(roles3);

        USER4 = new User();
        USER4.setId(5L);
        USER4.setName("USER4");
        USER4.setPassword("USER4");
        Set<Role> roles4 = USER4.getRoles();
        roles4.add(ROLE);
        USER4.setRoles(roles4);
    }
}
