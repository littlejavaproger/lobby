package com.getjavajob.nesterenkom.lobby.service;

import com.getjavajob.nesterenkom.lobby.model.*;
import com.getjavajob.nesterenkom.lobby.service.impl.LobbyServiceImpl;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class LobbyServiceImplImplTest extends LobbyServiceSupportTest {
//todo 6, 7
    private LobbyService lobbyService = LobbyServiceImpl.getInstance();

    @Test
    public void saveLobbyTest() {
        lobbyService.saveLobby(LOBBY1);
        lobbyService.saveLobby(LOBBY2);

        System.out.println("before uuid LOBBY1 [" + LOBBY1.getUuid() + "]");
        System.out.println("before uuid LOBBY2 [" + LOBBY2.getUuid() + "]");

        Lobby expected1 = LOBBY1;
        Lobby expected2 = LOBBY2;

        Lobby actual1 = lobbyService.findLobby(LOBBY1.getUuid());
        Lobby actual2 = lobbyService.findLobby(LOBBY2.getUuid());

        Assert.assertNotEquals(LOBBY1.getUuid(), LOBBY2.getUuid());
        assertThat(expected1, is(actual1));
        assertThat(expected2, is(actual2));
        Assert.assertNotNull(actual1.getUuid());

        lobbyService.removeLobby(LOBBY1);
        lobbyService.removeLobby(LOBBY2);
    }

    @Test
    public void saveNullLobbyTest() {
        Assert.assertNull(lobbyService.findLobby(LOBBY2.getUuid()));
    }

    @Test
    public void connectToLobbyWithTwoPlayersTest() {
        List<User> expected = new ArrayList<>();
        expected.add(USER_HOST);
        expected.add(USER1);
        lobbyService.saveLobby(LOBBY1);
        lobbyService.connectToLobby(LOBBY1.getUuid(), USER1);
        List<User> actual = lobbyService.findLobby(LOBBY1.getUuid()).getPlayers();

        assertThat(actual, is(expected));

        lobbyService.removeLobby(LOBBY1);
    }

    @Test
    public void connectToLobbyWithFourPlayersTest() {
        List<User> expected = new ArrayList<>();
        expected.add(USER_HOST);
        expected.add(USER1);
        expected.add(USER2);
        expected.add(USER3);
        lobbyService.saveLobby(LOBBY2);
        lobbyService.connectToLobby(LOBBY2.getUuid(), USER1);
        lobbyService.connectToLobby(LOBBY2.getUuid(), USER2);
        lobbyService.connectToLobby(LOBBY2.getUuid(), USER3);
        List<User> actual = lobbyService.findLobby(LOBBY1.getUuid()).getPlayers();

        assertThat(actual, is(expected));

        lobbyService.removeLobby(LOBBY2);
    }

    @Test
    public void getAllLobbyTest() {
        List<Lobby> expected = new ArrayList<>();
        expected.add(LOBBY1);
        expected.add(LOBBY2);

        lobbyService.saveLobby(LOBBY1);
        lobbyService.saveLobby(LOBBY2);
        List<Lobby> actual = lobbyService.getAllLobby();

        System.out.println("Size lobbybase expected " + expected.size());
        System.out.println("Size lobbybase actual " + actual.size());
        assertThat(actual, is(expected));

        lobbyService.removeLobby(LOBBY1);
        lobbyService.removeLobby(LOBBY2);
    }

    @Test
    public void checkNumberOfPlayerLobbyTest() {
        lobbyService.saveLobby(LOBBY1);

        assertThat(1, is(lobbyService.increaseNumberOfPlayers(LOBBY1)));

        lobbyService.connectToLobby(LOBBY1.getUuid(), USER1);

        assertThat(2, is(lobbyService.increaseNumberOfPlayers(LOBBY1)));

        lobbyService.removeLobby(LOBBY1);
    }

    @Test
    public void outputFromLobbyHost() {
        lobbyService.saveLobby(LOBBY1);
        lobbyService.connectToLobby(LOBBY1.getUuid(), USER1);
        lobbyService.outputToLobby(LOBBY1.getUuid(), LOBBY1.getHost());

        Assert.assertNull(lobbyService.findLobby(LOBBY1.getUuid()));

        lobbyService.removeLobby(LOBBY1);
    }

    @Test
    public void outputFromLobbyPlayer() {
        Lobby expected = LOBBY2;
        lobbyService.saveLobby(LOBBY2);
        lobbyService.connectToLobby(LOBBY2.getUuid(), USER1);
        lobbyService.connectToLobby(LOBBY2.getUuid(), USER2);
        lobbyService.connectToLobby(LOBBY2.getUuid(), USER3);
        lobbyService.outputToLobby(LOBBY1.getUuid(), USER2);
        lobbyService.outputToLobby(LOBBY1.getUuid(), USER1);
        Lobby actual = lobbyService.findLobby(LOBBY2.getUuid());

        assertThat(actual, is(expected));

        lobbyService.removeLobby(LOBBY2);
    }
}
