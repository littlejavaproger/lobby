package com.getjavajob.nesterenkom.lobby.service.impl;

import com.getjavajob.nesterenkom.lobby.model.Lobby;
import com.getjavajob.nesterenkom.lobby.model.TypeLobby;
import com.getjavajob.nesterenkom.lobby.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class LobbyServiceImpl implements com.getjavajob.nesterenkom.lobby.service.LobbyService {

    private static final Logger logger = LoggerFactory.getLogger(LobbyServiceImpl.class);
    private static LobbyServiceImpl instance;
    private ConcurrentMap<UUID, Lobby> lobbyBase = new ConcurrentHashMap<>();

    private LobbyServiceImpl() {
    }

    public static LobbyServiceImpl getInstance() {
        if (instance == null) {
            instance = new LobbyServiceImpl();
        }
        return instance;
    }

    //Метод создания лобби
    @Override
    public Lobby saveLobby(Lobby lobby) {
        logger.info("[Service] method saveLobby: before saving lobby %s", lobby);
        if (lobby.getName() == null || lobby.getHost() == null || lobby.getNumberOfPlayers() == 0 ||
                lobby.getType() == null || lobby.getStatus() == null) {
            logger.debug("[Service] method saveLobby: saving lobby name %s, host %s, number of Players %s, type lobby %s, status lobby %s",
                    lobby.getName(), lobby.getHost(), lobby.getNumberOfPlayers(), lobby.getType(), lobby.getStatus());
        }
        addHostForNumberOfPlayer(lobby);
        generateUUID(lobby);
        lobbyBase.put(lobby.getUuid(), lobby);
        logger.info("[Service] method saveLobby: after saving lobby %s", lobby);
        return lobby;
    }

    @Override
    public Lobby findLobby(UUID uuid) {
        if (findLobbyByUUID(uuid)) {
            return lobbyBase.get(uuid);
        }
        return null;
    }

    //Метод подключения Игрока к лобби
    @Override
    public Lobby connectToLobby(UUID uuid, User user) {
        if (uuid != null && user != null) {
            Lobby currentLobby = findLobby(uuid);
            if (checkSizeLobby(currentLobby)) {
                logger.info("[Service] method connectToLobby: The player connects to the lobby %s", user);
                List<User> users = currentLobby.getPlayers();
                users.add(user);
                currentLobby.setPlayers(users);
                logger.info("[Service] method connectToLobby: The list of players in the current lobby %s", currentLobby.getPlayers());
                increaseNumberOfPlayers(currentLobby);
            }
            return currentLobby;
        }
        return null;
    }

    //Метод выхода Игрока из лобби
    @Override
    public Lobby outputToLobby(UUID uuid, User user) {
        Lobby currentLobby = findLobby(uuid);
        if (user != null) {
            if (currentLobby.getHost().equals(user)) {
                return lobbyBase.remove(uuid);
            } else {
                List<User> users = currentLobby.getPlayers();
                users.remove(user);
                currentLobby.setPlayers(users);
                increaseNumberOfPlayers(currentLobby);
                return currentLobby;
            }
        }
        return null;
    }

    @Override
    public List<TypeLobby> getAllType() {

        return Arrays.asList(TypeLobby.values());
    }

    //Метод возвращает кол-во игроков в лобби
    @Override
    public int increaseNumberOfPlayers(Lobby lobby) {
        lobby.setNumberOfPlayers(lobby.getPlayers().size());

        return lobby.getNumberOfPlayers();
    }

    @Override
    public List<Lobby> getAllLobby() {
        List<Lobby> lobbies = new ArrayList<>();
        for (Lobby lobby : lobbyBase.values()) {
            lobbies.add(lobby);
        }
        return lobbies;
    }

    @Override
    public void removeLobby(Lobby lobby) {
        lobbyBase.remove(lobby.getUuid());
    }

    //Проверка свободных мест в лобби
    private boolean checkSizeLobby(Lobby lobby) {
        int sizeLobby = lobby.getPlayers().size();

        if (lobby.getType().equals(TypeLobby.TWO_PLAYERS)) {
            return sizeLobby < 2;
        } else if (lobby.getType().equals(TypeLobby.FOUR_PLAYERS)) {
            return sizeLobby < 4;
        }
        throw new SizeLobbyException("Free space in the lobby no");
    }

    private Lobby addHostForNumberOfPlayer(Lobby lobby) {
        List<User> users = new ArrayList<>();
        users.add(lobby.getHost());
        lobby.setPlayers(users);
        increaseNumberOfPlayers(lobby);

        return lobby;
    }

    //Генерирует UUID если такой существует в lobbyBase
    private Lobby generateUUID(Lobby lobby) {
        if (findLobbyByUUID(lobby.getUuid())) {
            lobby.setUuid(UUID.randomUUID());
            generateUUID(lobby);
        }
        return lobby;
    }

    //Ищет в lobbyBase лобби по UUID
    private boolean findLobbyByUUID(UUID uuid) {
        return lobbyBase.containsKey(uuid);
    }
}
