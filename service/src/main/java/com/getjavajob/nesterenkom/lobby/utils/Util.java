package com.getjavajob.nesterenkom.lobby.utils;

import com.getjavajob.nesterenkom.lobby.model.TypeLobby;

public class Util {

    public static TypeLobby convertStringToTypeLobby(String type){
        return TypeLobby.valueOf(type);
    }
}
