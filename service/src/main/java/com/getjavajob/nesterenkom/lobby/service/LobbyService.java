package com.getjavajob.nesterenkom.lobby.service;

import com.getjavajob.nesterenkom.lobby.model.Lobby;
import com.getjavajob.nesterenkom.lobby.model.TypeLobby;
import com.getjavajob.nesterenkom.lobby.model.User;

import java.util.List;
import java.util.UUID;

public interface LobbyService<E> {

    Lobby saveLobby(Lobby lobby);

    void removeLobby(Lobby lobby);

    Lobby findLobby(UUID uuid);

    List<Lobby> getAllLobby();

    Lobby connectToLobby(UUID uuid, User user);

    Lobby outputToLobby(UUID uuid, User user);

    int increaseNumberOfPlayers(Lobby lobby);

    List<TypeLobby> getAllType();
}
