package com.getjavajob.nesterenkom.lobby.service;

import java.util.List;

public interface GenericService<E> {

    E save(E entity);

    void delete(E entity);

    void delete(Long id);

    E edit(E entity);

    E find(Long id);

    List<E> findAll();
}
