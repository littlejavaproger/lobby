package com.getjavajob.nesterenkom.lobby.service;

public interface UserService<E> extends GenericService<E> {

    E login(String name, String password);

    E findUserByName(String name);
}
