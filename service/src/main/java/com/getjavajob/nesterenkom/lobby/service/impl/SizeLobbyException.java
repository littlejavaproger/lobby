package com.getjavajob.nesterenkom.lobby.service.impl;

public class SizeLobbyException extends RuntimeException{

    public SizeLobbyException() {
    }

    public SizeLobbyException(String message) {
        super(message);
    }

    public SizeLobbyException(String message, Throwable cause) {
        super(message, cause);
    }

    public SizeLobbyException(Throwable cause) {
        super(cause);
    }

    public SizeLobbyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
