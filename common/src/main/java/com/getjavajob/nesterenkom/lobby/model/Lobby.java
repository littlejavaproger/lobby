package com.getjavajob.nesterenkom.lobby.model;

import lombok.*;

import java.util.List;
import java.util.UUID;

@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
public
@Data
class Lobby {

    private UUID uuid;

    private String name;

    private TypeLobby type;

    private Status status;

    private List<User> players;

    private User host;

    private int numberOfPlayers;
}
