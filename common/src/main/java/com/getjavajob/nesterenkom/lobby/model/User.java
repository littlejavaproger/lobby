package com.getjavajob.nesterenkom.lobby.model;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@ToString
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users_tbl")
public
@Data
class User extends BaseEntity {

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "password", nullable = false)
    private String password;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "user_role_tbl", joinColumns = {
            @JoinColumn(name = "id_user", nullable = false, updatable = false)},
            inverseJoinColumns = {@JoinColumn(name = "id_role",
                    nullable = false, updatable = false)})
    private Set<Role> roles = new HashSet<>();
}
