package com.getjavajob.nesterenkom.lobby.model;

public enum TypeRole {
    ROLE_ADMIN, ROLE_USER
}
