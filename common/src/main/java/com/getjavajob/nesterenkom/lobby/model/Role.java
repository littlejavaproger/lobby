package com.getjavajob.nesterenkom.lobby.model;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@ToString(exclude = "users")
@EqualsAndHashCode(exclude = "users", callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "roles_tbl")
public
@Data
class Role extends BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Column(name = "role_name", nullable = false)
    @Enumerated(EnumType.STRING)
    private TypeRole roleName;

    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "roles")
    private Set<User> users;
}
