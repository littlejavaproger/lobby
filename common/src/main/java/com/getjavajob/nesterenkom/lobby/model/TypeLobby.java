package com.getjavajob.nesterenkom.lobby.model;

public enum TypeLobby {
    TWO_PLAYERS, FOUR_PLAYERS
}
