# **Lobby** #
#### Lobby for connection to the game
---
#### **Functionality:** ####
* authorization and authentication players
* create a lobby area with a choice of type for two or four players
* verification action
* connection and logout to the lobby created
* view a list of lobby sorted by type, name , status
* selection of the interface language to English or Russian
* JS/JQuery animation effects

#### **Tools: ** ####
JDK 8, Spring 4, Spring Security  JPA 2 / Hibernate 4, MySql, H2Database, JUnit 4, JSP/JSTL, Log4j, Maven 3, jQuery 1, Twitter Bootstrap 3, Jackson 2, Git / Bitbucket, Tomcat 8,  IntelliJIDEA 15.

---
#### **Nesterenko Maxim ** ####
Тренинг getJavaJob,  
http://www.getjavajob.com
![logo_1000x490.png](https://bitbucket.org/repo/xpnzXA/images/1173934031-logo_1000x490.png)
![welcome_page_1000x534.png](https://bitbucket.org/repo/xpnzXA/images/806059640-welcome_page_1000x534.png)
![create_lobby_1000x560.png](https://bitbucket.org/repo/xpnzXA/images/629630972-create_lobby_1000x560.png)
![access denied_1000x559.png](https://bitbucket.org/repo/xpnzXA/images/127349246-access%20denied_1000x559.png)
![all_lobbies_1000x535.png](https://bitbucket.org/repo/xpnzXA/images/447248423-all_lobbies_1000x535.png)