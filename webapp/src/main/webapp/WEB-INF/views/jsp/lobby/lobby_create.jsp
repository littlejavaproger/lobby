<%@include file="../common/taglib.jsp" %>
<html>
<head>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/css/fonts/favicon.ico"
          type="image/x-icon">
    <title><spring:message code="create_lobby.title" text="New lobby"/></title>
    <%@include file="../common/css.jsp" %>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/lobby_create.css">


</head>
<body>
<%@include file="../common/head-menu.jsp" %>
<form:form id="create-lobby-form" modelAttribute="lobby" action="${rootURL}lobby/doCreate" method="post"
           cssClass="form-horizontal">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><spring:message code="create_lobby.title" text="New lobby"/></h3>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="inputLobbyName" class="col-sm-3 control-label"><spring:message
                                    code="create_lobby.name_lobby" text="Name lobby"/></label>
                            <input id="name" type="text" name="name" class="form-control" id="inputLobbyName"
                                   placeholder="Lobby name"
                                   style="width: 50%;">
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><spring:message code="create_lobby.type_lobby"
                                                                                  text="Type"/></label>
                            <form:select id="type" path="type" cssClass="form-control" cssStyle="width: 50%;">
                                <form:option value="NONE"> --<spring:message code="create_lobby.select_type"
                                                                             text="SELECT"/>--</form:option>
                                <form:options items="${allTypeLobby}"></form:options>
                            </form:select>
                        </div>
                        <button id="action-btn" type="submit" class="btn btn-default" style="float: right">
                            <spring:message code="button.create" text="Save"/></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form:form>
<div class="modal fade" id="modal-confirm">
    <div class="modal-dialog" style="z-index: 1401">
        <div class="modal-content">
            <div class="modal-body te t-center">
                <h2><spring:message code="button.modal_message" text="Are you sure you want to save the lobby?"/></h2>
            </div>
            <div class="modal-footer center-block" style="text-align: center; padding: 20px">
                <button type="button" class="btn btn-raised btn-success" id="modal-save-lobby-btn"><spring:message
                        code="button.yes" text="Yes"/></button>
                <button type="button" class="btn btn-raised btn-danger" data-dismiss="modal"><spring:message
                        code="button.no" text="No"/></button>
            </div>
        </div>
    </div>
</div>
<%@include file="../common/footer.jsp" %>
<%@include file="../common/js.jsp" %>
<script src="${pageContext.request.contextPath}/resources/js/create_lobby.js"></script>
</body>
</html>
