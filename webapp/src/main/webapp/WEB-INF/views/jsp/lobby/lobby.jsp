<%@include file="../common/taglib.jsp" %>
<html>
<head>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/css/fonts/favicon.ico"
          type="image/x-icon">
    <title><spring:message code="lobby.title" text="Lobby page"/></title>
    <%@include file="../common/css.jsp" %>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/lobby_style.css">
</head>
<body>
<%@include file="../common/head-menu.jsp" %>
<table class="table table-striped">
    <tr>
        <th colspan="3" rowspan="2"><spring:message code="lobby.name" text="Name: "/> ${lobby.name}</th>
        <th class="status" rowspan="2"><spring:message code="lobby.status" text="Status: "/> ${lobby.status}</th>
    </tr>
    <tr></tr>
    <c:forEach var="player" items="${lobby.players}">
        <tr>
            <c:choose>
                <c:when test="${player.name.equals(lobby.host.name)}">
                    <td colspan="2"><img src="${pageContext.request.contextPath}/resources/img/superhero.gif"
                                         alt="Avatar" width="30px" height="30px"
                                         class="img-circle"> ${player.name} <spring:message code="lobby.host"
                                                                                            text="(Host)"/>
                    </td>
                </c:when>
                <c:otherwise>
                    <td colspan="2"><img src="${pageContext.request.contextPath}/resources/img/superhero.gif"
                                         alt="Avatar" width="30px" height="30px"
                                         class="img-circle"> ${player.name}</td>
                </c:otherwise>
            </c:choose>
        </tr>
    </c:forEach>
</table>
<div class="button_start">
    <button type="button" class="btn btn-success btn-lg" data-toggle="tooltip"
            data-placement="bottom"
            title="Start game"><a
            href='<c:url value="/lobby/start_game?id=${lobby.uuid}" />'><spring:message code="button.start_game"
                                                                                        text="Start game"/></a></button>
</div>
<div class="button_out">
    <button type="button" class="btn btn-danger btn-lg" data-toggle="tooltip"
            data-placement="bottom"
            title="output"><a
            href='<c:url value="/lobby/output?id=${lobby.uuid}" />'><spring:message code="button.logout"
                                                                                    text="Output"/></a></button>
</div>
<%@include file="../common/footer.jsp" %>
<%@include file="../common/js.jsp" %>
</body>
</html>
