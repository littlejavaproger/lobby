<%@include file="../common/taglib.jsp" %>
<form:form id="register-form" method="post" action="${rootURL}registration" modelAttribute="user"
           class="form-horizontal" role="form" cssStyle="display: none">
    <label class="label-form-reg">Username</label>
    <input type="text" name="username" class="username" placeholder="Username">
    <label class="label-form-reg">Password</label>
    <input type="password" name="password" class="password" placeholder="Password">

    <button type="submit">Sign me in</button>

    <input type="hidden" name="${_csrf.parameterName}"
           value="${_csrf.token}"/>
</form:form>