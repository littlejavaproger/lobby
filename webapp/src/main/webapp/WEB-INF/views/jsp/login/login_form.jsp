<%@include file="../common/taglib.jsp" %>
<form:form id="login-form" method="post" action="${rootURL}login" modelAttribute="user" class="form-horizontal"
           role="form">
    <label class="label-form-log">Username</label>
    <input type="text" name="username" class="username" placeholder="Username">
    <label class="label-form-log">Password</label>
    <input type="password" name="password" class="password" placeholder="Password">
    <div class="form-group text-center">
        <input type="checkbox" tabindex="3" class="" name="remember" id="remember">
        <label for="remember"> Remember Me</label>
    </div>
    <button type="submit">Sign me in</button>

    <input type="hidden" name="${_csrf.parameterName}"
           value="${_csrf.token}"/>
</form:form>