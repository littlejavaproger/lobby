<%@include file="../common/taglib.jsp" %>
<style>
    .error {
        padding: 15px;
        margin-bottom: 20px;
        border: 1px solid transparent;
        border-radius: 4px;
        color: #a94442;
        background-color: #f2dede;
        border-color: #ebccd1;
    }

    .msg {
        padding: 15px;
        margin-bottom: 20px;
        border: 1px solid transparent;
        border-radius: 4px;
        color: #31708f;
        background-color: #d9edf7;
        border-color: #bce8f1;
    }
</style>
<%--Error message--%>
<c:if test="${param.error != null}">
    <div class="error">
        <style scoped>
            .page-container {
                height: 600px;
            }
        </style>
        Invalid UserName and Password.
    </div>
</c:if>
<%--Info message--%>
<c:if test="${param.logout != null}">
    <div class="msg">
        You have been logged out.
        <style scoped>
            .page-container {
                height: 600px;
            }
        </style>
    </div>
</c:if>