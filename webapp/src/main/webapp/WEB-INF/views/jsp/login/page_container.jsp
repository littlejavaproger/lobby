<%@include file="../common/taglib.jsp" %>
<div class="page-container" id="id-container-log-reg">
    <h1>Lobby</h1>
    <hr>
    <div class="row">
        <div class="log-or-reg">
            <div class="col-xs-6">
                <a href="#" class="active" id="login-form-link"><h2>Login</h2></a>
            </div>
            <div class="col-xs-6">
                <a href="#" id="register-form-link"><h2>Registration</h2></a>
            </div>
        </div>
    </div>
    <hr>
    <%@include file="message.jsp" %>
    <%@include file="login_form.jsp" %>
    <%@include file="registration_form.jsp" %>
</div>