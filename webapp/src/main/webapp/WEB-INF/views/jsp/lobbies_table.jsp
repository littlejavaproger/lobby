<%@include file="common/taglib.jsp" %>
<html>
<head>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/css/fonts/favicon.ico"
          type="image/x-icon">
    <title>Lobby Table</title>
    <%@include file="common/css.jsp" %>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/datatables.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/lobbies_table.css">
    <%--<link rel="stylesheet" type="text/css"--%>
          <%--href="https://cdn.datatables.net/t/bs-3.3.6/jq-2.2.0,dt-1.10.11,b-1.1.2,fc-3.2.1,fh-3.1.1,kt-2.1.1,rr-1.1.1,sc-1.4.1,se-1.1.2/datatables.min.css"/>--%>
    <link rel="stylesheet" type="text/css"
          href="https://cdn.datatables.net/t/bs-3.3.6/jq-2.2.0,dt-1.10.11,b-1.1.2,fc-3.2.1,fh-3.1.1,kt-2.1.1,rr-1.1.1,sc-1.4.1,se-1.1.2/datatables.min.css"/>
    <%@include file="common/css.jsp" %>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/lobbies_table.css">
</head>
<body>
<%@include file="common/head-menu.jsp" %>
<div class="all_lobbies" style="padding-top: 100px;">
    <table id="lobby-table" class="table table-striped">
        <thead>
        <tr>
            <td>#</td>
            <td><spring:message code="lobbies.name" text="Lobbies name"/></td>
            <td><spring:message code="lobbies.host" text="Host"/></td>
            <td><spring:message code="lobbies.number_of_player" text="Number of player"/></td>
            <td><spring:message code="lobbies.type" text="Type"/></td>
            <td><spring:message code="lobbies.status" text="Status"/></td>
            <td><spring:message code="lobbies.action" text="Action"/></td>
        </tr>
        </thead>
        <tfoot>
        <tr>
            <td>#</td>
            <td><spring:message code="lobbies.name" text="Lobbies name"/></td>
            <td><spring:message code="lobbies.host" text="Host"/></td>
            <td><spring:message code="lobbies.number_of_player" text="Number of player"/></td>
            <td><spring:message code="lobbies.type" text="Type"/></td>
            <td><spring:message code="lobbies.status" text="Status"/></td>
            <td><spring:message code="lobbies.action" text="Action"/></td>
        </tr>
        </tfoot>
        <tbody>
        <c:forEach var="lobby" items="${lobbies}">
            <c:set var="count" value="${count + 1}" scope="page"/>
            <tr class="lobby_row">
                    <%--Numbering--%>
                <td><c:out value="${count}"/></td>
                    <%--Name lobby--%>
                <td>${lobby.name}</td>
                    <%--Host lobby--%>
                <td>${lobby.host.name}</td>
                    <%--Number_of_players--%>
                <td>${lobby.numberOfPlayers}</td>
                    <%--Type lobby--%>
                <td>${lobby.type}</td>
                    <%--Status lobby--%>
                <td>${lobby.status}</td>
                    <%--Connection--%>
                <td>
                  <c:if test="${(lobby.numberOfPlayers < 2 && lobby.type == 'TWO_PLAYERS') || (lobby.numberOfPlayers < 4 && lobby.type == 'FOUR_PLAYERS')}">
                      <div class="button_actions">
                          <button id="action_btn" type="button" class="btn btn-default btn-lg" data-toggle="tooltip"
                                  data-placement="bottom"
                                  title="Connection for lobby"><a
                                  href='<c:url value="/lobby/connection?id=${lobby.uuid}" />'><spring:message
                                  code="button.connection" text="Connection for lobby"/></a>
                          </button>
                      </div>
                  </c:if>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>
<%@include file="common/footer.jsp" %>
<%@include file="common/js.jsp" %>
<script src="${pageContext.request.contextPath}/resources/js/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script>
    $(document).ready(function () {
        $('#lobby-table').DataTable();
    });
</script>
</body>
</html>
