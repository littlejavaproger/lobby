<!DOCTYPE html>
<%@page import="com.getjavajob.nesterenkom.lobby.controllers.UserController" %>
<%@ page import="com.getjavajob.nesterenkom.lobby.model.User" %>
<%@include file="common/taglib.jsp" %>
<html>
<head>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/css/fonts/favicon.ico"
          type="image/x-icon">
    <title><spring:message code="welcome.title" text="Welcome page"/></title>

    <%@include file="common/css.jsp" %>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/welcome-page.css">
</head>
<body>
<%@include file="common/head-menu.jsp" %>
<%--todo 3--%>
<%
    User user = UserController.getCurrentUser();
    String username = UserController.getCurrentUser().getName();
    String password = UserController.getCurrentUser().getPassword();
    session = request.getSession();
    session.setAttribute("user", user);
    session.setAttribute("username", username);
    session.setAttribute("password", password);
%>
<div class="welcome-frame">
    <div class="welcome-page">
        <h2><spring:message code="welcome.name" text="Name: "/> <sec:authentication property="name"/></h2>
        <h2><spring:message code="welcome.message" text="Welcome to the game!"/></h2>
    </div>
</div>
<%@include file="common/footer.jsp" %>
<%@include file="common/js.jsp" %>
</body>
</html>