<nav class="navbar navbar-default navbar-custom navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/welcome"><spring:message code="menu.lobby"
                                                                                                      text="Lobby"/></a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="active"><a href="${pageContext.request.contextPath}/welcome"><span
                        class="glyphicon glyphicon-home"
                        aria-hidden="true"></span> <spring:message code="menu.home" text="Home"/></a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span
                            class="glyphicon glyphicon-folder-open" aria-hidden="true"></span> <spring:message
                            code="menu.pages" text="Pages"/><span
                            class="caret"></span></a>
                    <ul class="dropdown-menu pull-right">
                        <sec:authorize access="hasRole('ROLE_USER')">
                            <li><a href="${pageContext.request.contextPath}/users/online"><spring:message
                                    code="menu.pages_showOnLinePlayers" text="Show players on-line"/></a></li>
                            <li><a href="${pageContext.request.contextPath}/lobby/all_info"><spring:message
                                    code="menu.pages_showAllLobbies" text="Show all lobbies"/></a></li>
                            <li><a href='<c:url value="/lobby/create" />'><spring:message code="menu.pages_CreateLobby"
                                                                                          text="Create lobby"/></a></li>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <li><a href="${pageContext.request.contextPath}/admin"><spring:message
                                    code="menu.pages_Administration" text="Administration page"/></a></li>
                        </sec:authorize>
                    </ul>
                </li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="?language=ru"><spring:message code="menu.language_ru" text="Rus"/></a></li>
                <li><a href="?language=en"><spring:message code="menu.language_eng" text="Eng"/></a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                        <sec:authentication property="name"/>
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="#"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span> <spring:message
                                code="menu.settings" text="Setting"/></a>
                        </li>
                        <li><a href="${pageContext.request.contextPath}/logout"><span class="glyphicon glyphicon-off"
                                                                                      aria-hidden="true"></span>
                            <spring:message code="menu.logout" text="logout"/></a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
