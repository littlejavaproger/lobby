<!DOCTYPE html>
<%@include file="common/taglib.jsp" %>
<html>
<head>
    <%--todo 4--%>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/css/fonts/favicon.ico"
          type="image/x-icon">
    <meta charset="utf-8">
    <title>Login page</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <%--CSS--%>
    <%@include file="common/css.jsp" %>
    <%@include file="login/css.jsp" %>
</head>
<body>
<%@ include file="login/page_container.jsp" %>
<%@ include file="common/footer.jsp" %>
<!-- Javascript -->
<%@include file="common/js.jsp" %>
<script src="${pageContext.request.contextPath}/resources/js/login/supersized.3.2.7.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/login/supersized-init.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/login/log_scripts.js"></script>
</body>
</html>
