//Notification
$("#action-btn").click(function (e) {
    e.preventDefault();
    var validationOk = true;
    if (validationOk == true) {
        $('#modal-confirm').modal('show');
    }
    else {
        $('.top-right').notify({
            message: {text: errorText.val},
            fadeOut: {enabled: true, delay: 3000},
            closable: false,
            type: 'danger'
        }).show();
    }
});

$("#modal-save-lobby-btn").click(function (e) {
    $("#create-lobby-form").submit();
    e.preventDefault();
});

//check for empty data
$(document).ready(function () {
    $("#action-btn").attr('disabled', 'disabled');
    $("form").keyup(function () {
        // To Disable Submit Button
        $("#action-btn").attr('disabled', 'disabled');
        var name = $("#name").val();

        $("#type").on('change', function () {
            if (!(name == "")) {
                $("#action-btn").removeAttr('disabled');
                $("#action-btn").css({
                    "cursor": "pointer",
                    "box-shadow": "1px 0px 6px #333"
                });
            }
        });
    })
});