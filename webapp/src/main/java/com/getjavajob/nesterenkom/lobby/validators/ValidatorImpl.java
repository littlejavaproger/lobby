package com.getjavajob.nesterenkom.lobby.validators;

import com.getjavajob.nesterenkom.lobby.model.Lobby;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class ValidatorImpl implements Validator {

    @Override
    public boolean supports(Class aClass) {
        return Lobby.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Lobby lobby = (Lobby) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "type", "required.type");

        if ("NONE".equals(lobby.getName())) {
            errors.rejectValue("name", "required.name");
        }
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "required.name");
    }
}
