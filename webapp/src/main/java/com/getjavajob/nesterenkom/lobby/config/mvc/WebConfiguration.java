package com.getjavajob.nesterenkom.lobby.config.mvc;

import com.getjavajob.nesterenkom.lobby.config.ServiceConfiguration;
import com.getjavajob.nesterenkom.lobby.config.security.SecurityConfiguration;
import com.getjavajob.nesterenkom.lobby.validators.ValidatorImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.validation.Validator;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

import java.util.Properties;

@Configuration
@ComponentScan(value = {"com.getjavajob.nesterenkom.lobby"})
@EnableWebMvc
@Import({ServiceConfiguration.class, SecurityConfiguration.class, LanguageSelectionConfiguration.class})
public class WebConfiguration extends WebMvcConfigurerAdapter {

    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/resources/");
    }

    @Bean
    public InternalResourceViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver
                = new InternalResourceViewResolver();
        viewResolver.setViewClass(JstlView.class);
        viewResolver.setPrefix("/WEB-INF/views/jsp/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }

    @Bean
    public SimpleMappingExceptionResolver simpleMappingExceptionResolver() {
        SimpleMappingExceptionResolver b = new SimpleMappingExceptionResolver();
        Properties mappings = new Properties();
        mappings.put("org.springframework.dao.DataAccessException", "error");
        b.setExceptionMappings(mappings);
        return b;
    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        super.addViewControllers(registry);
        registry.addViewController("login").setViewName("login");
        registry.addViewController("welcome").setViewName("welcome-test");
        registry.addViewController("admin").setViewName("admin-test");
        registry.addViewController("users/online").setViewName("online");
    }

//    Validator
    @Override
    public Validator getValidator() {
        return new ValidatorImpl();
    }

}
