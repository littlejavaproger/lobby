package com.getjavajob.nesterenkom.lobby.controllers;

import com.getjavajob.nesterenkom.lobby.model.Lobby;
import com.getjavajob.nesterenkom.lobby.model.Status;
import com.getjavajob.nesterenkom.lobby.model.TypeLobby;
import com.getjavajob.nesterenkom.lobby.model.User;
import com.getjavajob.nesterenkom.lobby.service.LobbyService;
import com.getjavajob.nesterenkom.lobby.service.impl.LobbyServiceImpl;
import com.getjavajob.nesterenkom.lobby.utils.Util;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Controller
@RequestMapping("lobby")
public class LobbyController {
    private static final Logger logger = LoggerFactory.getLogger(LobbyController.class);

    private LobbyService lobbyService = LobbyServiceImpl.getInstance();

    @RequestMapping(value = "/all_info", method = RequestMethod.GET)
    public String getAllLobby(Model model) {
        List<Lobby> lobbies = lobbyService.getAllLobby();
        model.addAttribute("lobbies", lobbies);

        return "lobbies_table";
    }

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String employeeCreate(Model model) {
        model.addAttribute("lobby", new Lobby());
        model.addAttribute("allTypeLobby", lobbyService.getAllType());


        return "lobby/lobby_create";
    }

    @RequestMapping(value = "/doCreate", method = RequestMethod.POST)
    public void lobbyDoCreate(@RequestParam String type,
                              @ModelAttribute("lobby") Lobby lobby,
                              HttpServletResponse response,
                              HttpServletRequest request,
                              Model model) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");

        HttpSession session = request.getSession(true);
        User host = (User) session.getAttribute("user");
        lobby.setHost(host);
        lobby.setType(Util.convertStringToTypeLobby(type));
        lobby.setStatus(Status.RECRUITED_PARTICIPANTS);
        UUID uuid = UUID.randomUUID();
        lobby.setUuid(uuid);
        model.addAttribute("lobby", lobby);
        lobbyService.saveLobby(lobby);

        response.sendRedirect(request.getContextPath() + "/lobby/connection?id=" + uuid);
    }

    @RequestMapping(value = "/connection", method = RequestMethod.GET)
    public ModelAndView connectionToLobby(@RequestParam(value = "id") UUID uuid,
                                          HttpServletRequest request,
                                          HttpServletResponse response) throws IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");

        Lobby lobby = lobbyService.findLobby(uuid);
        logger.info("[WEB] method connectionToLobby: The lobby that connects %s", lobby.getName());
        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute("user");

        logger.info("[WEB] method connectionToLobby: The player connects to the lobby %s", user);
        if (lobby != null) {
            if (!user.equals(lobby.getHost())) {
                lobbyService.connectToLobby(uuid, user);
            }
        } else if ((lobby.getNumberOfPlayers() == 2 && lobby.getType().equals(TypeLobby.TWO_PLAYERS)) || (lobby.getNumberOfPlayers() == 4 && lobby.getType().equals(TypeLobby.FOUR_PLAYERS))) {
            response.sendRedirect(request.getContextPath() + "/lobby/all_info");
        } else {
            response.sendRedirect(request.getContextPath() + "/lobby/all_info");
        }

        ModelAndView model = new ModelAndView("lobby/lobby");
        model.addObject("lobby", lobby);

        return model;
    }

    @RequestMapping(value = "/output", method = RequestMethod.GET)
    public void outputToLobby(@RequestParam(value = "id") UUID uuid, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Lobby lobby = lobbyService.findLobby(uuid);

        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute("user");

        logger.info("[WEB] method outputToLobby: Users output for lobby %s", user);

        if (user.equals(lobby.getHost())) {
            lobbyService.removeLobby(lobby);
            response.sendRedirect(request.getContextPath() + "/lobby/all_info");

        } else {
            lobbyService.outputToLobby(uuid, user);
            response.sendRedirect(request.getContextPath() + "/lobby/all_info");
        }
    }

    @RequestMapping(value = "/start_game", method = RequestMethod.GET)
    public void startGameToLobby(@RequestParam(value = "id") UUID uuid, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Lobby lobby = lobbyService.findLobby(uuid);

        if ((lobby.getType().equals(TypeLobby.TWO_PLAYERS) && lobby.getNumberOfPlayers() == 2) || (lobby.getType().equals(TypeLobby.FOUR_PLAYERS) && lobby.getNumberOfPlayers() == 4)) {
            lobby.setStatus(Status.THE_GAME);
            response.sendRedirect(request.getContextPath() + "/lobby/connection?id=" + lobby.getUuid());
        }

        HttpSession session = request.getSession(true);
        User user = (User) session.getAttribute("user");
    }
}
