package com.getjavajob.nesterenkom.lobby.controllers;

import com.getjavajob.nesterenkom.lobby.config.security.SecurityUser;
import com.getjavajob.nesterenkom.lobby.model.User;
import com.getjavajob.nesterenkom.lobby.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;

@Controller
public class UserController {

    private static UserService<User> userService;

    @Autowired
    public void setUserService(UserService userService) {
        UserController.userService = userService;
    }

    public static User getCurrentUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            String username = ((UserDetails) principal).getUsername();
            User loginUser = userService.findUserByName(username);
            return new SecurityUser(loginUser);
        }
        return null;
    }
}
